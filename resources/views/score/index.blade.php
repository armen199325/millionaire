@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row justify-content-center align-items-center">
            @foreach($scores as $score)
                <div class="col-md-8 bg-white mt-3 rounded">
                    <div class="row">
                        <div class="col-6">
                            <h5>Score</h5>
                        </div>
                        <div class="col-6">
                            <h5>{{$score->score}}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <h5>User</h5>
                        </div>
                        <div class="col-6">
                            <h5>{{$score->user->name}} {{$score->user->surname}}</h5>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section("scripts")
    <script type="application/javascript" src="{{asset('js/game_script.js')}}"></script>
@endsection
