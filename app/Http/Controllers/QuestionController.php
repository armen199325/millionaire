<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Validator;

class QuestionController extends Controller
{
    //
    public function index(){
        $questions = Question::getQuestions();
        $maxPoint = Question::MAX_POINT;
        $minPoint = Question::MIN_POINT;
        return view("question/index", compact("questions", "maxPoint", "minPoint"));
    }

    public function store(Request $request){
        $maxPoint = Question::MAX_POINT;
        $minPoint = Question::MIN_POINT;
        $validation = Validator::make($request->all(), [
            "question" => "required|string",
            "answer" => "required",
            "right-answers" => "required",
            "points" => "integer | between:$minPoint,$maxPoint"
        ]);
        if($validation->fails()){
            return ["success" => false, "messages" => $validation->messages()];
        }
        $question = new Question();
        $question->question = $request->input("question");
        $question->answers = json_encode($request->input("answer"));
        $question->right_answers = json_encode($request->input("right-answers"));
        $question->points = $request->input("points");
        $question->save();
        return ["success" => true];
    }

    public function destroy($id){
        $question = Question::find($id);
        $question->delete();
        return redirect()->back();
    }

    public function show($id){
        $minPoint = Question::MIN_POINT;
        $maxPoint = Question::MAX_POINT;
        $question = Question::find($id);
        return view("question/show", compact("question", "minPoint", "maxPoint"));
    }

    public function edit($id, Request $request){
        $minPoint = Question::MIN_POINT;
        $maxPoint = Question::MAX_POINT;
        $validation = Validator::make($request->all(), [
            "question" => "string",
            "points" => "integer |  between:$minPoint,$maxPoint"
        ]);
        if($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }
        $question = Question::find($id);
        $question->question = $request->input("question");
        $question->answers = json_encode($request->input("answer"));
        $question->right_answers = json_encode($request->input("right-answers"));
        $question->points = $request->input("points");
        $question->update();
        return redirect()->back();
    }

    public function check($id, Request $request){
        $question = Question::find($id);
        $answer = $request->input("answerNumber");
        $rightQuestions = json_decode($question->right_answers);
        return ["success" => in_array($answer, $rightQuestions)];
    }
}
