<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    const PAGINATION_COUNT = 15;
    const MIN_POINT = 5;
    const MAX_POINT = 20;

    public static function getQuestions(){
        return Question::orderBy("id", "desc")->paginate(self::PAGINATION_COUNT);
    }
}
