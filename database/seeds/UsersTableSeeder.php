<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User();
        $user->name = "Admin";
        $user->email = "admin@admin.com";
        $user->surname = "Admin";
        $user->password = Hash::make("12345678");
        $user->role_id = Role::ADMIN_ROLE_ID;
        $user->save();
    }
}
