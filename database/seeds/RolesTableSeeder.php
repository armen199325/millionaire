<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role = new Role();
        $role->id = Role::USER_ROLE_ID;
        $role->name = "User";
        $role->slug = "user";
        $role->save();
        $adminRole = new Role();
        $adminRole->id = Role::ADMIN_ROLE_ID;
        $adminRole->name = "Admin";
        $adminRole->slug = "admin";
        $adminRole->save();
    }
}
