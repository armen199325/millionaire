@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 bg-white pt-3">
                <form action="{{ route("questions.edit", [$question->id])}}" method="POST">
                    @csrf
                    @method("PUT")
                    <div class="form-group">
                        <label for="question">Question</label>
                        <input type="text" id="question" name="question" class="form-control" value="{{$question->question}}">
                    </div>
                    @php $answers = json_decode($question->answers) @endphp
                    <div class="answers">
                        @foreach($answers as $index => $answer)
                            <div class="form-group">
                                <label for="answer">Answer</label>

                                @if($index > 1)
                                    <div class="input-group">
                                        <input id="answer" type="text" class="form-control default-answer new-created" value="{{$answer}}" required name="answer[]">
                                        <div class="input-group-append cursor-pointer input-delete">
                                            <span class="input-group-text">X</span>
                                        </div>
                                    </div>
                                @else
                                    <input type="text" name="answer[]" id="question" class="default-answer form-control" value="{{$answer}}">
                                @endif
                            </div>
                        @endforeach
                    </div>
                    <button type="button" class="btn btn-primary add-new-answer">Add New Answer</button>
                    <div class="form-group mt-3">
                        <label for="right-answers">Choose Right Answers</label>
                        <select name="right-answers[]" class="form-control" id="right-answers" multiple>
                            @php $right_answers = json_decode($question->right_answers) @endphp
                            @for($i = 1; $i <= count($answers); $i++)
                                <option value="{{$i}}" @if(in_array($i, $right_answers)) selected @endif>{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="points">Points</label>
                        <input id="points" type="number" min="{{$minPoint}}" max="{{$maxPoint}}" value="{{$question->points}}" class="form-control" name="points" required>
                    </div>
                    <div class="mb-3">
                        <button class="btn btn-primary" type="submit">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script type="application/javascript" src="{{asset('js/game_script.js')}}"></script>
@endsection
