<?php

namespace App\Http\Controllers;

use App\Score;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ScoreController extends Controller
{
    //
    public function index(){
        $scores = Score::orderBy("score", "desc")->with("user")->paginate(15);
        return view("score/index", compact("scores"));
    }

    public function store(Request $request){
        $score = new Score();
        $score->user_id = Auth::id();
        $score->score = $request->input("points");
        $score->save();
        return ["success" => true];
    }
}
