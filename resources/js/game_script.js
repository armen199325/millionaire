const defaultAnswersCount = 2;
let answersCount = $(".default-answer").length;
let points = 0;
let questionPage = 1;

$(".toast").toast();

$(".add-question-button").on("click", function(){
    let modal = $("#question-modal");
    modal.modal("show");
});
$(".question-modal-form").on("submit", function(event){
    event.preventDefault();

    $.ajax({
        type: "POST",
        url: '/question',
        data: $(this).serialize(),
        success: function(data){
            console.log(data);
            $("#question-modal").modal("hide");
            resetModalForm();
            if(data.success === true){
                $(".toast").toast("show");
                setTimeout(() => {
                    location.reload();
                }, 2000)
            } else {
                alert("Please Enter Valid Information")
            }
        },
        error: function(data){
            console.error(data)
        }
    })
});
$(".add-new-answer").on("click", function(){
    answersCount ++;
    $(".answers").append(`
        <div class="form-group">
            <label for="answer">Answer</label>
            <div class="input-group">
                <input id="answer" type="text" class="form-control new-created" required name="answer[]">
                <div class="input-group-append cursor-pointer input-delete">
                  <span class="input-group-text">X</span>
                </div>
            </div>
        </div>
    `);
    $("#right-answers").append(`<option class="added-option" id="answer-${answersCount}" value='${answersCount}'>${answersCount}</option>`);
});
$(document).on("click", ".input-delete", function(){
    $(this).parents(".form-group").remove();
    $("#answer-" + answersCount).remove();
    answersCount --;
});

function resetModalForm(){
    let modalForm = $(".question-modal-form");
    modalForm.find(".new-created").parents(".form-group").remove();
    modalForm.find('input').val("");
    modalForm.find(".added-option").remove();
    answersCount = defaultAnswersCount;
    modalForm.find("option").attr("selected", false);
}
$(".delete-question").on("click", function () {
    let csrfTocken = $('meta[name="csrf-token"]').attr('content');
    console.log($(this).data("action"));
    $.ajax({
        url: $(this).data("action"),
        type: "POST",
        data: {
            _token: csrfTocken,
            _method: "delete",
        },
        success: function(){
            location.reload();
        },
        error: function(data){
            console.error(data)
        }
    })
});
$(".choose-answer").on("click", function(){
    let questionId = $(this).data("id");
    let answerNumber = $(this).data("number");
    let answerPoints = $(this).data("points");
    checkAnswer(questionId, answerNumber, answerPoints);
});

function checkAnswer(questionId, answerNumber, answerPoints){
    $.ajax({
        url: `/question/check/${questionId}`,
        type: "GET",
        data: {
            answerNumber: answerNumber,
        },
        success: function(data){
            if(data.success){
                points += answerPoints;
                showCorrectMessage();
            } else {
                showErrorMessage();
            }
        },
        error: function(data){
            console.error(data)
        }
    });
}

function showCorrectMessage(){
    //todo
    alert("correct");
    showNextQuestion();
}

function showErrorMessage(){
    //todo
    alert("error");
    showNextQuestion();
}

function showNextQuestion(){
    console.log(points);
    questionPage ++;
    let tab = $(`#pills-${questionPage}`);
    if(tab.length){
        $(".tab-pane").removeClass("active");
        tab.tab("show");
    } else {
        submitPoints();
    }
}
function submitPoints(){
    $.ajax({
        url: "/points",
        type: "get",
        data: {
            points: points,
        },
        success: function(data){
            if(data.success){
                alert("Your score is submitted");
                location.reload();
            }
        },
        error: function(data){
            console.error(data)
        }
    })
}
