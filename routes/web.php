<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/game', ["uses" => "GameController@index", "as" => "game", "middleware" => "auth"]);
Route::get("/question", ["uses" =>"QuestionController@index", "as" => "question", "middleware" => ["auth", "isAdmin"]]);
Route::post("/question", ["uses" => "QuestionController@store", "as" => "questions.store", "middleware" => ["auth", "isAdmin"]]);
Route::delete("/question/{id}", ["uses" => "QuestionController@destroy", "as" => "questions.destroy", "middleware" => ["auth", "isAdmin"]]);
Route::get("/question/{id}/show", ["uses" => "QuestionController@show", "as" => "questions.show", "middleware" => ["auth", "isAdmin"]]);
Route::put("/question/{id}/edit", ["uses" => "QuestionController@edit", "as" => "questions.edit", "middleware" => ["auth", "isAdmin"]]);
Route::get("/question/check/{id}", ["uses" => "QuestionController@check", "as" => "questions.check", "middleware" => "auth"]);
Route::get("/points", ["uses" => "ScoreController@store", "as" => "score.save", "middleware" => "auth"]);
Route::get("/score", ["uses" => "ScoreController@index", "as" => "score.show", "middleware" => "auth"]);
