@extends("layouts.app")

@section("content")
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-8 bg-white">

            {{--<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Home</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
            @if(count($questions) < 5)
                <div class="col-md-8 my-3 rounded">
                    <h3 class="text-center text-white">The Game Haven't Enough Questions, Please Contact With Support</h3>
                </div>
            @else
                <div class="tab-content" id="pills-tabContent">
                        @foreach($questions as $question)
                        <div class="tab-pane fade show @if($loop->first) active @endif" id="pills-{{$loop->iteration}}" role="tabpanel" aria-labelledby="pills-home-tab">
                            <h5 class="mt-3">Question</h5>
                            <h5>{{$question->question}}</h5>
                            <div class="row @if($loop->last) last-row @endif">
                                @foreach(json_decode($question->answers) as $answer)
                                    <div class="col-md-6"><p data-id="{{$question->id}}" data-points="{{$question->points}}" data-number="{{$loop->iteration}}" class="text-center rounded p-2 cursor-pointer choose-answer border">{{$answer}}</p></div>
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">...</div>
                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">...</div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section("scripts")
    <script type="application/javascript" src="{{asset('js/game_script.js')}}"></script>
@endsection
