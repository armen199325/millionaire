<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
    //

    public function index(){
        $questions = Question::inRandomOrder()->take(5)->get();
        return view("game", compact("questions"));
    }
}
