@extends("layouts.app")

@section("content")
    <div class="container">
        @if(count($questions) !== 0)
            <div class="row justify-content-center align-items-center">
                <div class="col-md-8 my-3 rounded">
                    <h3 class="text-center text-white">Questions</h3>
                </div>
            </div>
            @foreach($questions as $question)
                <div class="row justify-content-center align-items-center">
                    <div class="col-md-8 bg-white mt-3 rounded" style="min-height: 200px">
                        <h5 class="fw-500 mt-3">Question</h5>
                        <h5>{{$question->question}}</h5>
                        <h5 class="mt-3">Answers</h5>
                        <div class="row">
                            @foreach(json_decode($question->answers) as $answer)
                                <div class="col-md-6"><p class="text-center border">{{$answer}}</p></div>
                            @endforeach
                        </div>
                        <div class="mb-3">
                            <a href="{{route("questions.show", [$question->id])}}" class="btn btn-primary">Edit Question</a>
                            <button class="btn btn-danger delete-question" data-action="{{route("questions.destroy", [$question->id])}}">Delete Question</button>
                        </div>

                    </div>
                </div>
            @endforeach
        @else
            <div class="row justify-content-center align-items-center">
                <div class="col-md-8 my-3 rounded">
                    <h3 class="text-center text-white">There Aren't Any Question</h3>
                </div>
            </div>
        @endif

        {{ $questions->links() }}
        <div class="row  justify-content-center align-items-center">
            <div class="col-md-8 mt-3">
                <button type="button" class="btn btn-primary add-question-button" >
                    Add New Question
                </button>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="question-modal" tabindex="-1" role="dialog" aria-labelledby="question-modal-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="question-modal-form" action="">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add New Question</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="question">Question</label>
                            <input id="question" type="text" class="form-control" name="question" required>
                        </div>
                        <div class="answers">
                            <div class="form-group">
                                <label for="answer">Answer</label>
                                <input id="answer" type="text" class="form-control default-answer" name="answer[]" required>
                            </div>
                            <div class="form-group">
                                <label for="answer">Answer</label>
                                <input id="answer" type="text" class="form-control default-answer" name="answer[]" required>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary add-new-answer">Add New Answer</button>
                        <div class="form-group mt-3">
                            <label for="right-answers">Choose Right Answers</label>
                            <select name="right-answers[]" class="form-control" id="right-answers" multiple>
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="points">Points</label>
                            <input id="points" type="number" min="{{$minPoint}}" max="{{$maxPoint}}" class="form-control" name="points" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary save-question">Save Question</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div aria-live="polite" aria-atomic="true" style="position: relative">
        <div class="toast" role="alert" aria-live="assertive" data-delay="5000" aria-atomic="true" style="position: absolute; bottom: 50px; right: 50px;">
            <div class="toast-header">
                <strong class="mr-auto toast-header-message">Success</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                The Question is Added Successfully
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script type="application/javascript" src="{{asset('js/game_script.js')}}"></script>
@endsection
