/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/game_script.js":
/*!*************************************!*\
  !*** ./resources/js/game_script.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var defaultAnswersCount = 2;
var answersCount = $(".default-answer").length;
var points = 0;
var questionPage = 1;
$(".toast").toast();
$(".add-question-button").on("click", function () {
  var modal = $("#question-modal");
  modal.modal("show");
});
$(".question-modal-form").on("submit", function (event) {
  event.preventDefault();
  $.ajax({
    type: "POST",
    url: '/question',
    data: $(this).serialize(),
    success: function success(data) {
      console.log(data);
      $("#question-modal").modal("hide");
      resetModalForm();

      if (data.success === true) {
        $(".toast").toast("show");
        setTimeout(function () {
          location.reload();
        }, 2000);
      } else {
        alert("Please Enter Valid Information");
      }
    },
    error: function error(data) {
      console.error(data);
    }
  });
});
$(".add-new-answer").on("click", function () {
  answersCount++;
  $(".answers").append("\n        <div class=\"form-group\">\n            <label for=\"answer\">Answer</label>\n            <div class=\"input-group\">\n                <input id=\"answer\" type=\"text\" class=\"form-control new-created\" required name=\"answer[]\">\n                <div class=\"input-group-append cursor-pointer input-delete\">\n                  <span class=\"input-group-text\">X</span>\n                </div>\n            </div>\n        </div>\n    ");
  $("#right-answers").append("<option class=\"added-option\" id=\"answer-".concat(answersCount, "\" value='").concat(answersCount, "'>").concat(answersCount, "</option>"));
});
$(document).on("click", ".input-delete", function () {
  $(this).parents(".form-group").remove();
  $("#answer-" + answersCount).remove();
  answersCount--;
});

function resetModalForm() {
  var modalForm = $(".question-modal-form");
  modalForm.find(".new-created").parents(".form-group").remove();
  modalForm.find('input').val("");
  modalForm.find(".added-option").remove();
  answersCount = defaultAnswersCount;
  modalForm.find("option").attr("selected", false);
}

$(".delete-question").on("click", function () {
  var csrfTocken = $('meta[name="csrf-token"]').attr('content');
  console.log($(this).data("action"));
  $.ajax({
    url: $(this).data("action"),
    type: "POST",
    data: {
      _token: csrfTocken,
      _method: "delete"
    },
    success: function success() {
      location.reload();
    },
    error: function error(data) {
      console.error(data);
    }
  });
});
$(".choose-answer").on("click", function () {
  var questionId = $(this).data("id");
  var answerNumber = $(this).data("number");
  var answerPoints = $(this).data("points");
  checkAnswer(questionId, answerNumber, answerPoints);
});

function checkAnswer(questionId, answerNumber, answerPoints) {
  $.ajax({
    url: "/question/check/".concat(questionId),
    type: "GET",
    data: {
      answerNumber: answerNumber
    },
    success: function success(data) {
      if (data.success) {
        points += answerPoints;
        showCorrectMessage();
      } else {
        showErrorMessage();
      }
    },
    error: function error(data) {
      console.error(data);
    }
  });
}

function showCorrectMessage() {
  //todo
  alert("correct");
  showNextQuestion();
}

function showErrorMessage() {
  //todo
  alert("error");
  showNextQuestion();
}

function showNextQuestion() {
  console.log(points);
  questionPage++;
  var tab = $("#pills-".concat(questionPage));

  if (tab.length) {
    $(".tab-pane").removeClass("active");
    tab.tab("show");
  } else {
    submitPoints();
  }
}

function submitPoints() {
  $.ajax({
    url: "/points",
    type: "get",
    data: {
      points: points
    },
    success: function success(data) {
      if (data.success) {
        alert("Your score is submitted");
        location.reload();
      }
    },
    error: function error(data) {
      console.error(data);
    }
  });
}

/***/ }),

/***/ 1:
/*!*******************************************!*\
  !*** multi ./resources/js/game_script.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\OSPanel\domains\millionaire\resources\js\game_script.js */"./resources/js/game_script.js");


/***/ })

/******/ });